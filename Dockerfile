FROM node:current-alpine
RUN addgroup -S app && adduser -SDH -g "" -G app app
COPY code /code
RUN ls -alh /code
RUN cd /code/api && npm run setup && cd /code/web && npm run setup
WORKDIR /code/api
USER app
EXPOSE 8000
ENTRYPOINT ["npm", "start"]